#pragma once

#include "common\TreeNode.h"

using namespace std;

class Solution {
public:
	vector<vector<int>> levelOrder(TreeNode* root) {
		std::vector<std::vector<int>> result;
		auto levelNodeList = std::make_shared<std::queue<TreeNode*, std::list<TreeNode*>>>();
		auto levelNodeList2 = std::make_shared<std::queue<TreeNode*, std::list<TreeNode*>>>();

		if (root != nullptr){
			levelNodeList->push(root);
		}

		while (!levelNodeList->empty()){
			std::vector<int> levelResult;

			while (!levelNodeList->empty()){
				TreeNode* node = levelNodeList->front();
				levelNodeList->pop();

				levelResult.push_back(node->val);

				if (node->left){
					levelNodeList2->push(node->left);
				}

				if (node->right){
					levelNodeList2->push(node->right);
				}
			}

			result.push_back(levelResult);

			std::swap(levelNodeList, levelNodeList2);
		}

		return result;
	}
};
