#pragma once

#include <vector>
#include <list>
#include <deque>
#include <queue>
#include <string>
#include <iostream>

#include <memory>
#include <utility>
#include <algorithm>

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

TreeNode* getTreeNode(int value){
	TreeNode* node = new TreeNode(value);
	node->left = nullptr;
	node->right = nullptr;

	return node;
}

TreeNode* buildTree(){
	TreeNode* root = new TreeNode(1);
	root->left = getTreeNode(2);
	root->right = getTreeNode(3);
	
	root->left->left = getTreeNode(4);
	root->right->left = getTreeNode(6);

	root->left->left->right = getTreeNode(12);

	return root;
}
