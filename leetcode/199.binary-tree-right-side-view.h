#pragma once

#include "common\TreeNode.h"

using namespace std;

class Solution {
public:
	vector<int> rightSideView(TreeNode* root) {
		std::vector<int> result;

		visitNode(root, 1, result);

		return result;
	}

	void visitNode(TreeNode* root, int currentDepth, std::vector<int>& result){
		if (root == nullptr){
			return;
		}

		if (currentDepth > result.size()){
			result.push_back(root->val);
		}

		visitNode(root->right, currentDepth + 1, result);
		visitNode(root->left, currentDepth + 1, result);
	}
};